# Krutadnyata Trust's Website

## Adding dropdowns

```yml
menu:
  -
    label: Programs
    url: programs
    dropdowns:
      -
        url: u1
        label: l1
        dropdowns:
          -
            url: u1.1
            label: l1.1
            dropdowns:
              -
                url: u1.1.1
                label: l1.1.1
              -
                url: u1.1.2
                label: l1.1.2
          -
            url: u1.2
            label: l1.2
```
