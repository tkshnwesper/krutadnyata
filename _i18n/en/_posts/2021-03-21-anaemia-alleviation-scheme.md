---
layout: post
title:  "Anaemia Alleviation Scheme"
---

## Project title

Anemia Alleviation Scheme (AAS) for Tribal Children
of South Bastar (Chattisgadh)

## Aim

To diagnose and treat anemia, manage resulting complication, and create awareness for its prevention and malnutrition in inaccessible and difficult terrain of south Bastar, Chattisgadh.

## Details

Dr. Ramchandra Godbole and his wife, Suneeta, is an exemplary couple that has dedicated entire life to selfless service of our Vanavasi brethren. Hailing from a well-to-do family in Satara, Maharashtra Dr Godbole, ably supported by Suneeta, took a courageous and unusual decision to devote his life for providing medical assistance to the honest, hardworking but illiterate vanavasis living in abject generational poverty. The Godboles have spent over three decades – spread across some of the most remote, treacherous and economically backward districts of Maharashtra and Chhattisgadh – with a single-minded focus on their life’s mission.

After spending over 10 years at Barsoor (Dist. Bastar, Chhattisgadh), Dr. Godbole came back to Maharashtra in early 2000 to serve under the auspices of Vanavasi Kalyan Ashram for the next 10 years. However, aware of the appalling medical healthcare conditions in some of the hostile, Naxalite-infested, far flung forest areas around Barsoor, he decided to return to his ‘karma bhoomi’ for the second time in 2010.
On their return Godboles saw that poor road conditions and lack of availability of transport compounded the apathy amongst the locals to get
the sick and ailing to the distant medical centers. Therefore, realizing the urgent need, he started conducting medical camps in surrounding interior villages and remote hamlets.

Fortunately, Dr. Godbole found like-minded partners and colleagues ready to work shoulder-to-shoulder with equal passion and commitment for this worthy cause. Dr. Mukund Karmalkar, a Hyderabad based Cardiologist, joined Dr. Godbole in 2015; and in the same year a young, 22-years old, local tribal Chituram Vek came forward to work with Dr. Godbole.

The trio, with the support and help of other well-wishers, arranged 83 medical camps successfully in surrounding areas of Dantewnda, Sukama and Bijapur districts of South Bastar in the last 6 years. More than 7,000 patients benefitted from these camps.
With years of public-spirited service and unmatched commitment, Dr. Godbole and the charitable organization he has set up – Vanwasi Vikas Samiti Barsoor (Dantewada) Chattisgadh – have won the heart and trust of the shy and reticent locals. Given the continued need for reach out of medical assistance in far-flung, isolated areas Dr. Godbole plans to continue organising medical camps in different villages, with special focus on 6 to 12 years old children.

In addition, given the widespread prevalence of malnutrition and anemia amongst the vanavasis, he has launched “Anemia Alleviation Scheme” for tribal children from 1st Jan 2021 (A brief write-up on the project is attached - Annexure 1). Such a scheme assumes great significance, especially for children as early affliction impedes their growth, affects their ability to attend school, resulting in large number of dropouts and thus propagating a vicious cycle of poor health -> illiteracy -> poverty- > chronic malnutrition -> ailments like anemia.

We, at Krutadnyata Trust have been indeed fortunate to be associated for over a decade with the remarkable but arduous work of Dr. Godbole and will also be supporting his new initiative on anemia eradication.
Given the scale of the problem and the deep-rooted underlying issues, it is going to be long, uphill task. However, with dedication and tireless work of Dr. Godbole and his colleagues it is certainly an achievable target.
An ambitious project like this certainly requires significant resources and financial support. (Project cost details are attached in annexure 2) We have always found that there are many compassionate and benevolent people like you who willingly come forward to offer a helping hand for such noble, voluntary work. Through this post we are soliciting your generous support for this deserving project.

Donation details are [here](/donate).